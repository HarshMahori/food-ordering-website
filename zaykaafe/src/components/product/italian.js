import "./product.css";

function product() {
  return (
    <section className="menu container mx-auto pt-8 pb-11">
      <h1 className="text-3xl border-b pb-1 font-semibold mb-8">Italian</h1>
      <h2 className="text-lg pb-1 font-semibold mb-8">Pizza</h2>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 col-gap-12 gap-y-11">
        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/Margherita.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">MARGHERITA</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> REGULAR </option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹245</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/farmhouse.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">FARM HOUSE</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> REGULAR </option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹265</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/peppypaneer.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">PEPPY PANEER</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> REGULAR </option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹265</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/mexican.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">MEXICAN GREEN WAVE</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> REGULAR </option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹285</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/deluxe.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">DELUXE VEGGIE</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> REGULAR </option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹219</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-50 mb-4 mx-auto"
            src="/images/indi.jpg"
            width={200}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">INDI TANDOORI PANEER</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected>REGULAR</option>
                <option> LARGE </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹305</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <h2 className="text-lg pb-1 font-semibold my-8">Pasta</h2>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 gap-x-10 gap-y-11">
        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-40 mb-4 mx-auto"
            src="/images/pasta1.jpg"
            width={375}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">MOROCCAN SPICE PASTA VEG</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> HALF </option>
                <option> FULL </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹145</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-40 mb-4 mx-auto"
            src="/images/pasta2.jpg"
            width={375}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">TIKKA MASALA PASTA VEG</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> HALF </option>
                <option> FULL </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹129</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>

        <div className="rounded-2xl hover:shadow-lg transition duration-300 ease-in-out">
          <img
            className="h-40 mb-4 mx-auto"
            src="/images/pasta3.jpg"
            width={375}
            alt=""
          />
          <div className="text-center">
            <h2 className="mb-4 text-lg">CREAMY TOMATO PASTA VEG</h2>
            <div className="flex items-center mx-20">
              <select className="py-1 rounded-xl text-xs bg-slate-100 hover:bg-slate-200">
                <option selected> HALF </option>
                <option> FULL </option>
              </select>
            </div>
            <div className="flex items-center justify-around mt-6 mb-6">
              <span className="font-bold text-lg">₹145</span>
              <button className="add-to-cart hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 rounded-full flex items-center">
                <span className="font-medium">+</span>
                <span className="ml-4 font-medium">Add</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default product;
