import "./additem.css";
import React, { useState } from "react";
// import { useNavigate } from "react-router-dom';
import axios from "axios";

function additem() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [admin, setAdmin] = useState({
    name: "",
    desc: "",
    quantity: "",
    price: "",
  });

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [error, setError] = useState("");
  let name, value;

  const handleInputs = (e) => {
    name = e.target.name;
    value = e.target.value;

    setAdmin({ ...admin, [name]: value });
  };

  const AddProduct = async (e) => {
    e.preventDefault();

    try {
      const url = "/item/additem";
      const { data: res } = await axios.post(url, admin);
      console.log(res.message);
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }
    }
  };
  return (
    <div class="wrapper bg-white mt-sm-5">
      <h4 class="pb-4 border-bottom">Add Product</h4>
      <div class="d-flex align-items-start py-3 border-bottom">
        <img
          src="https://images.pexels.com/photos/1037995/pexels-photo-1037995.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          class="img"
          alt=""
        />
        <div class="pl-sm-4 pl-2" id="img-section">
          <h6>Product Image</h6>
          <p>Accepted file type .png. Less than 1MB</p>
          <input type="file" id="file"></input>
          <button class="ml-auto border rounded py-1 px-3 bg-blue-100  hover:bg-blue-500 hover:text-white">
            {" "}
            Upload{" "}
          </button>
        </div>
      </div>
      <div class="py-2">
        <div class="row py-2">
          <div class="col-md-12">
            <label for="name"> Product Name</label>
            <input
              type="text"
              class="bg-light form-control"
              placeholder=" Product Name"
              value={admin.name}
              onChange={handleInputs}
              name="name"
            />
          </div>
        </div>

        <div class="py-2">
          <div class="row py-2">
            <div class="col-md-12">
              <label for="name"> Product Details</label>
              <input
                type="text"
                class="bg-light form-control"
                placeholder=" Product Details"
                value={admin.desc}
                onChange={handleInputs}
                name="desc"
              />
            </div>
          </div>

          <div class="row py-2">
            <div class="col-md-6">
              <label for="name"> Product Quantity</label>
              <div class="arrow">
                <select name="quantity" id="quantity" class="bg-light">
                  <option
                    value={admin.quantity}
                    onChange={handleInputs}
                    name="quantity"
                  >
                    Half
                  </option>
                  <option
                    value={admin.quantity}
                    onChange={handleInputs}
                    name="quantity"
                  >
                    Full
                  </option>
                  <option
                    value={admin.quantity}
                    onChange={handleInputs}
                    name="quantity"
                  >
                    Small
                  </option>
                  <option
                    value={admin.quantity}
                    onChange={handleInputs}
                    name="quantity"
                  >
                    Large
                  </option>
                </select>
              </div>
            </div>
            {/* <input type="text" class="bg-light form-control" placeholder=" Product Details"/> */}

            <div class="col-md-6">
              <label for="name"> Product Price</label>
              <input
                type="text"
                class="bg-light form-control"
                placeholder=" Product Price"
                value={admin.price}
                onChange={handleInputs}
                name="price"
              />
            </div>
          </div>
          <div class="py-3 pb-4 border-bottom">
            <button
              onClick={AddProduct}
              class="bg-orange-400 hover:bg-orange-500 text-white rounded transition duration-300 py-2 px-2 mr-2"
            >
              Add Product
            </button>
            <button class="btn border button py-2">Cancel</button>
          </div>
          <div class="d-sm-flex align-items-center pt-3" id="delete">
            <div>
              <b>Delete this product</b>
            </div>
            <div class="ml-auto">
              <button class="bg-red-500 hover:bg-red-600  text-white rounded transition duration-300 py-2 px-3 ">
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default additem;
