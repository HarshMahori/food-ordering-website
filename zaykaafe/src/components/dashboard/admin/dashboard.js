import "./dashboard.css";
import axios from "axios";
import React, { useEffect, useState } from "react";

function Dashboard() {
  const [category, setCategory] = useState([]);

  const getCategory = async () => {
    const response = await axios.get("/category/getcategory");
    console.log(response.data.data);
    setCategory(await response.data.data);
  };

  useEffect(() => {
    getCategory();
  }, []);

  const setData = (data) => {
    let { _id, name, subname, desc } = data;
    localStorage.setItem("ID", _id);
    localStorage.setItem("name", name);
    localStorage.setItem("subname", subname);
    localStorage.setItem("desc", desc);
  };

  //   const getData = async () => {
  //     const response = await axios.get("/category/getcategory");
  //     setCategory(await response.data.data);
  //   };

  //   const getData = () => {
  //     axios.get("/category/getcategory").then((getData) => {
  //       console.log(getData.data.data);
  //       setCategory(getData.data.data);
  //     });
  //   };

  const onDelete = (_id) => {
    axios.delete(`/category/deletecategory/${_id}`).then(() => {
      getCategory();
    });
  };

  return (
    <>
      <section className="my-10">
        <h2 className="mx-16 pb-4 font-semibold border-b-zinc-400 border-b">
          Categories
        </h2>

        <a
          href="/addcategory"
          className="bg-primary1 decoration-transparent mx-16 mt-2 hover:bg-primaryhover rounded-full text-white font-bold py-2 px-6 focus:outline-none focus:shadow-outline"
          type="button"
        >
          Add Category
        </a>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 col-gap-12 gap-y-11 mx-16 mt-10 mb-14">
          {category.map((cate) => {
            return (
              <div>
                <div className="rounded-2xl hover:shadow-2xl border-transparent border-2 hover:border-neutral-300 hover:border-solid transition duration-300 ease-in-out w-full md:w-64">
                  <img
                    className="h-40 mb-4 mx-auto"
                    src={`/uploads/${cate.imgpath}`}
                    alt=""
                  />
                  <div className="text-center content p-7">
                    <div className="title">{cate.name}</div>
                    <div className="sub-title">{cate.subname}</div>
                    <div className="bottom">
                      <p>{cate.desc}</p>
                    </div>
                    <div className="bottom flex items-center justify-around mt-6 mb-6">
                      <a href={"/updatecategory/" + cate._id}>
                        <button
                          onClick={() => setData(cate)}
                          className="hover:bg-primary1 hover:border-pure hover:text-pure transition duration-200 py-1 px-6 font-medium items-center add-to-cart"
                        >
                          <span className="font-medium">Modify</span>
                        </button>
                      </a>
                      <button
                        onClick={() => onDelete(cate._id)}
                        className="hover:bg-red-600 hover:border-pure border-red-600 delete text-red-600 hover:text-pure transition duration-200 py-1 px-6 font-medium items-center"
                      >
                        <span className="font-medium">Delete</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
}

export default Dashboard;
