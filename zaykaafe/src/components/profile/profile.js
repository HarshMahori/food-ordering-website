import "./profile.css";
// import React, { useState } from "react";
// import { useNavigate } from "react-router-dom";
// import axios from "axios";

function profile() {
  // const navigate = useNavigate();
  // const [user, setUser] = useState({
  //   name: "",
  //   email: "",
  //   number: "",
  //   address: "",
  // });

  // let name, value;

  // const handleInputs = (e) => {
  //   name = e.target.name;
  //   value = e.target.value;

  //   setUser({ ...user, [name]: value });
  // };

  // const PostData = (e) => {
  //   e.preventDefault();
  //   const { name, email, number, address } = user;
  //   axios
  //     .post("/user/updateuser", {
  //       name,
  //       email,
  //       number,
  //       address,
  //     })
  //     .then(() => {
  //       navigate("/login");
  //     });
  //   };

  //   const DeleteData = (e) => {
  //       e.preventDefault();
  //       const { name, email, number, address } = user;
  //       axios
  //         .post("/user/deleteuser", {
  //           name,
  //           email,
  //           number,
  //           address,
  //         })
  //         .then(() => {
  //           navigate("/login");
  //         });
  //       };

  return (
    <div className="wrapper bg-white mt-sm-5">
      <h4 className="pb-4 border-bottom">Account settings</h4>
      <div className="d-flex align-items-start py-3 border-bottom">
        <img
          src="https://images.pexels.com/photos/1037995/pexels-photo-1037995.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          className="img"
          alt=""
        />
        <div className="pl-sm-4 pl-4" id="img-section">
          <b>Profile Photo</b>
          <p>Accepted file type .png. Less than 1MB</p>
          <input type="file" id="file"></input>
          <button className="mt-3 border rounded py-1 px-3 bg-blue-100  hover:bg-blue-500 hover:text-white">
            Upload
          </button>
        </div>
      </div>
      <div className="py-2">
        <div className="row py-2">
          <div className="col-md-12">
            <label for="name"> Name</label>
            <input
              type="text"
              className="bg-light form-control"
              // value={user.name}
              // onChange={handleInputs}
              placeholder="Name"
            />
          </div>
        </div>
        <div className="row py-2">
          <div className="col-md-6">
            <label for="email">Email Address</label>
            <input
              type="text"
              className="bg-light form-control"
              // value={user.email}
              // onChange={handleInputs}
              placeholder="zaykaa@gmail.com"
            />
          </div>
          <div className="col-md-6 pt-md-0 pt-3">
            <label for="phone">Phone Number</label>
            <input
              type="tel"
              className="bg-light form-control"
              // value={user.number}
              // onChange={handleInputs}
              placeholder="+91 9999999999"
            />
          </div>
        </div>
        <div className="row py-2">
          <div className="col-md-12">
            <label for="address"> Address</label>
            <textarea
              type="text"
              className="bg-light form-control"
              // value={user.address}
              // onChange={handleInputs}
              placeholder="Address"
            />
          </div>
        </div>
        <div className="row py-2">
          <div className="col-md-6">
            <label for="country">Country</label>
            <select name="country" id="country" className="bg-light">
              <option value="india" selected>
                India
              </option>
              <option value="usa">USA</option>
              <option value="uk">UK</option>
              <option value="uae">UAE</option>
            </select>
          </div>
          <div className="col-md-6 pt-md-0 pt-3" id="lang">
            <label for="language">Language</label>
            <div className="arrow">
              <select name="language" id="language" className="bg-light">
                <option value="english" selected>
                  English
                </option>
                <option value="english_us">English (United States)</option>
                <option value="enguk">English UK</option>
                <option value="arab">Arabic</option>
              </select>
            </div>
          </div>
        </div>
        <div className="py-3 pb-4 border-bottom">
          <button className="bg-orange-400 hover:bg-orange-500 text-white rounded transition duration-300 py-2 px-2 mr-2">
            Save Changes
          </button>
          <button className="btn border button py-2">Cancel</button>
        </div>
        <div className="d-sm-flex align-items-center pt-3" id="deactivate">
          <div>
            <b>Delete your account</b>
            <p>Details about your company account and password</p>
          </div>
          <div className="ml-auto">
            <button className="bg-red-500 hover:bg-red-600  text-white rounded transition duration-300 py-2 px-3 ">
              Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default profile;
