import "./cart.css";
import ecart from "../../images/ecart.PNG";
// import cart from "../../images/cart-black.png";

function Cart() {
  return (
    <section className="cart py-16">
      {/* <div className="mx-auto lg:w-1/2">
        <div className="container">
          <div className="flex items-center border-b border-gray-300 pb-4">
            <img src={cart} alt="cart" />
            <h1 className="font-bold ml-4 text-2xl">Order summary</h1>
          </div>
          <div className="pizza-list">
            
            <div className="flex items-center my-8">
              <img className="w-24" src="#" alt="" />
              
              <div className="flex-1 ml-4">
               
                <h1 className="text-lg">Margherita</h1>
                <span>SMALL</span>
              </div>
              
              <span className="flex-1">1 Pcs</span>
              <span className="font-bold text-lg">₹300</span>
            </div>

            <div className="flex items-center my-8">
              <img className="w-24" src="/img/<%= pizza.item.image %>" alt="" />
              <div className="flex-1 ml-4">
                
                <h1 className="text-lg">Margherita</h1>
                <span>SMALL</span>
              </div>
              
              <span className="flex-1">1 Pcs</span>
              <span className="font-bold text-lg">₹300</span>
            </div>

            <div className="flex items-center my-8">
              <img className="w-24" src="/img/<%= pizza.item.image %>" alt="" />
              <div className="flex-1 ml-4">
               
                <h1 className="text-lg">Margherita</h1>
                <span>SMALL</span>
              </div>
              
              <span className="flex-1">1 Pcs</span>
              <span className="font-bold text-lg">₹300</span>
            </div>
            
          </div>
          <hr />
          <div className="text-right">
            <div>
              <span className="text-lg font-bold">Total Amount:</span>
              
              <span className="amount text-2xl font-bold ml-2 text-primary1">
                ₹300
              </span>
            </div>
            
            <div>
              <form
                
                className="mt-12"
                
              >
                <div className="relative w-1/2 ml-auto mb-4">
                  <select
                    
                    className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
                  >
                    <option value="cod">Cash on delivery</option>
                    
                  </select>
                  <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg
                      className="fill-current h-4 w-4"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                    >
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                  </div>
                </div>
                <input
                  name="phone"
                  className="border border-gray-400 p-2 w-1/2 mb-4"
                  type="text"
                  placeholder="Phone number"
                />
                <br />
                <input
                  name="address"
                  className="border border-gray-400 p-2 w-1/2"
                  type="text"
                  placeholder="Address"
                />
                <div className="mt-4">
                  
                  <button
                    className="bg-primary1 hover:bg-primaryhover rounded-full text-white font-bold py-2 px-6 focus:outline-none focus:shadow-outline"
                    type="submit"
                  >
                    Order Now
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> */}
      <div className="mb-36">
        <div>
          <div className="empty-cart-cls text-center ">
            <img
              src={ecart}
              width={270}
              height={270}
              className="mx-auto mt-4"
            />
            <p className="text-xl text-neutral-600 font-semibold mb-1">
              Your cart is empty
            </p>
            <p className="text-neutral-500 text-sm">
              You can go to home page to view more cuisines or food
            </p>
            <br></br>
            <a
              href="/"
              className="bg-orange-500 hover:bg-orange-600 rounded text-white font-bold py-2 px-7 focus:outline-none focus:shadow-outline decoration-transparent transition duration-300"
            >
              CONTINUE SHOPPING
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Cart;
