import "./header.css";
import logo from "../../images/bikelogo.png";

const Home = () => {
  const handleLogout = () => {
    localStorage.removeItem("token");
    window.location = "/login";
  };

  return (
    <section className="sticky top-0 z-50">
      <nav className="mx-auto flex items-center justify-between py-1 shadow-sm appearance-none bg-white">
        <div className="ml-1 shrink-0 md:ml-6 lg:ml-8 xl:ml-10 w-56 md:w-60 lg:w-52 xl:w-48 icon">
          <a href="/">
            <img src={logo} alt="logo" />
          </a>
        </div>

        {/* <div className="flex justify-center pl-16 hidden md:block">
          <div className="mt-3 w-96">
            <div className="input-group relative flex flex-wrap items-stretch w-full mb-3 rounded">
              <input
                type="search"
                className="relative flex-auto appearance-none border rounded px-3 py-1.5 leading-tight text-gray-700 focus:outline-none focus:shadow-outline"
                placeholder="Search for food"
                aria-label="Search"
                aria-describedby="button-addon2"
              />
              <div className="pl-2 py-2">
                <span id="basic-addon2">
                  <svg
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="fas"
                    data-icon="search"
                    className="w-4"
                    role="img"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                  >
                    <path
                      fill="currentColor"
                      d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"
                    ></path>
                  </svg>
                </span>
              </div>
            </div>
          </div>
        </div> */}

        <div className="pt-3 mr-10">
          <ul className="flex items-center">
            <li className="ml-9">
              <a
                href="/italian"
                className="text-dark decoration-transparent font-semibold"
              >
                Menu
              </a>
            </li>
            <li className="ml-9">
              <a
                href="/offer"
                className="text-dark decoration-transparent font-semibold"
              >
                Offers
              </a>
            </li>
            <li className="ml-9">
              <a
                href="/review"
                className="text-dark decoration-transparent font-semibold"
              >
                Review
              </a>
            </li>
            <li className="ml-9">
              <a
                href="/profile"
                className="text-dark decoration-transparent font-semibold"
              >
                Profile
              </a>
            </li>
            <li className="ml-9">
              <button
                className="text-dark decoration-transparent font-semibold"
                onClick={handleLogout}
              >
                Log out
              </button>
            </li>
            <div
              className="fas fa-bars px-2.5 py-2 bg-zinc-200 text-xl hover:text-slate-50
            rounded transition duration-200 text-zinc-800 hidden"
              id="menu-btn"
            ></div>
            <li className="ml-9">
              <a href="/checkout">
                <div
                  className="fas fa-shopping-cart shopping-cart text-xl px-2 py-2 bg-zinc-200 hover:bg-orange-500 hover:text-slate-50
              rounded transition duration-200 text-zinc-800"
                ></div>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </section>
  );
};

export default Home;
