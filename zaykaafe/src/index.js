import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Dashboard from "./components/dashboard/user/dashboard";
import Home from "./components/header/home";
import AdminDashboard from "./components/dashboard/admin/dashboard";
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import Review from "./components/review/review";
import Offer from "./components/offer/offer";
import About from "./components/about/about";
import Cart from "./components/cart/cart";
import Profile from "./components/profile/profile";
import AddItem from "./components/dashboard/admin/additem";
import AddCategory from "./components/dashboard/admin/addcategory";
import UpdateCategory from "./components/dashboard/admin/updatecategory";
import Italian from "./components/product/italian";
import Login from "./components/auth/user/login";
import AdminLogin from "./components/auth/admin/adminlogin";
import Signup from "./components/auth/user/signup";
import reportWebVitals from "./reportWebVitals";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/jquery/dist/jquery.min.js";
import "../node_modules/bootstrap/dist/js/bootstrap.min.js";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
const user = localStorage.getItem("token");
// const admin = localStorage.getItem("token1");
root.render(
  // <React.StrictMode>
  //   <Header/>
  // <App />
  // </React.StrictMode>

  <Router>
    {!user && <Header />}
    {user && <Home />}
    {/* {!admin && <Header />} */}
    {/* {admin && <Home />} */}
    <Routes>
      <Route path="/" element={<Dashboard />} />
      <Route path="/additem" element={<AddItem />} />
      <Route path="/addcategory" element={<AddCategory />} />
      <Route path="/updatecategory/:_id" element={<UpdateCategory />} />
      <Route path="/admin/home" element={<AdminDashboard />} />
      <Route path="/login" element={<Login />} />
      <Route path="/admin/login" element={<AdminLogin />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/checkout" element={<Cart />} />
      <Route path="/profile" element={<Profile />} />
      <Route path="/italian" element={<Italian />} />
      <Route path="/review" element={<Review />} />
      <Route path="/about" element={<About />} />
      <Route path="/offer" element={<Offer />} />
    </Routes>
    <Footer />
  </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
