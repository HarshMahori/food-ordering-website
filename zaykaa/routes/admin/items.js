var express = require("express");
var router = express.Router();
var Item = require("../../models/item");

router.post("/additem", async (req, res) => {
  try {
    let item = await new Item(req.body).save();
    res.json({
      message: "Item Added Successfully",
      data: item,
      success: true,
    });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

router.get("/getitem", async (req, res) => {
  try {
    const getitem = await Item.find().exec();
    res.json({ message: "Item Details", data: getitem, success: true });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

module.exports = router;
