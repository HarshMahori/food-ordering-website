var express = require("express");
var router = express.Router();
var User = require("../models/user");
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const { verifyEmail } = require("../utils/JWT");

let {
  encryptPassword,
  comparePasswords,
  generateJwt,
} = require("../utils/auth");

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.post("/adduser", async (req, res) => {
  try {
    let user = await new User(req.body).save();
    res.json({ message: "User Added Successfully", data: user, success: true });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

router.get("/getuser", async (req, res) => {
  try {
    const getuser = await User.find().exec();
    res.json({ message: "User Details", data: getuser, success: true });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

router.post("/updateuser", async (req, res) => {
  try {
    let user = await User.findByIdAndUpdate(req.body.id, {
      name: req.body.name,
    }).exec();
    res.json({
      message: "User Successfully Updated",
      data: user,
      success: true,
    });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

router.post("/deleteuser", async (req, res) => {
  try {
    await User.findByIdAndRemove(req.body.id).exec();
    res.json({ message: "Successfully Deleted", success: true });
  } catch (err) {
    res.json({ message: err.message, success: false });
  }
});

//Email AUTH

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "anmol.garg594@gmail.com",
    pass: process.env.PASS,
  },
  tls: {
    rejectUnauthorized: false,
  },
});

// User Register API starts here

router.post("/register", async (req, res) => {
  try {
    const { name, email, number, password } = req.body;
    const user = new User({
      name,
      email,
      number,
      password,
      emailToken: crypto.randomBytes(64).toString("hex"),
      isVerified: false,
    });

    const userEmailCheck = await User.findOne({
      email: new RegExp(`^${req.body.email}$`, "i"),
    }).exec();

    const userNumberCheck = await User.findOne({
      number: new RegExp(`^${req.body.number}$`, "i"),
    }).exec();

    if ((!name, !email, !number, !password))
      return res.status(400).send({ message: "All Fields are Required" });

    // console.log(userEmailChk);
    if (userEmailCheck)
      return res.status(401).send({ message: "Email Already Registered" });

    // console.log(userNumberChk);
    if (userNumberCheck)
      return res
        .status(402)
        .send({ message: "Mobile Number Already Registered" });

    req.body.password = await encryptPassword(req.body.password);

    let newUser = await new User(req.body).save();

    //send verification mail to user
    var mailOptions = {
      from: ' "Verify your email" <anmol.garg594@gmail.com> ',
      to: user.email,
      subject: "Zaykaa -verify your email",
      html: `<h2> ${user.name}! Thanks for registering on our site </h2>
            <h4> Please verify your mail to continue.... </h4>
            <a href = "http://${req.headers.host}/users/verify-email?token-${user.emailToken}"> Verfiy Your Email </a>`,
    };

    //sending email
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Verification email is sent to your gmail acccount");
      }
    });

    res.status(200).json({
      message: "User Register Successfully",
      data: user,
      success: true,
    });
  } catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });
    else res.json({ message: "Error", data: err, success: false });
  }
});

//   User Register API Close

//Email Verification

router.get("/verify-email", async (req, res) => {
  try {
    const token = req.query.token;
    const user = await User.findOne({ emailToken: token });
    if (user) {
      user.emailToken = null;
      user.isVerified = true;
      await user.save();
      console.log("Account Verified");
    } else {
      console.log("Email is not verified");
    }
  } catch (err) {
    console.log(err);
  }
});

// User Login API Starts Here

router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({
      email: new RegExp(`^${req.body.email}$`, "i"),
    }).exec();

    if (!user) return res.status(401).json({ message: "Email not registered" });

    if ((!email, !password))
      return res.status(400).send({ message: "All Fields are Required" });

    const checkPassword = await comparePasswords(
      req.body.password,
      user.password
    );

    if (!checkPassword)
      return res.status(402).send({ message: "Invalid Password" });

    const token = await generateJwt(User._id);
    res.status(200).send({ data: token, message: "Logged in successfully" });
  } catch (err) {
    console.error(err);
    if (err.message) res.status(500).send({ message: "Internal Server Error" });
    else res.json({ message: "Error", data: err, success: false });
  }
});

// User Login Api Ends

module.exports = router;
