var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var user = new Schema(
  {
    role: {
      type: String,
      default: "customer",
    },
    name: {
      type: String,
      required: true,
      maxlength: 100,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    number: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 10,
    },
    address: {
      type: String,
      maxlength: 100,
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
    },
    emailToken: {
      type: String,
    },
    isVerified: {
      type: Boolean,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", user);
