var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ItemSchema = new Schema({
  //   img: {
  //     type: String,
  //     required: true,
  //   },
  name: {
    type: String,
    required: true,
    maxlength: 60,
  },
  //   size: {
  //     type: [
  //       {
  //         text: { type: String, required: true },
  //         price: { type: Number, required: true },
  //       },
  //     ],
  //   },
  desc: {
    type: String,
    required: true,
  },
  quantity: {
    type: String,
    // required: true,
  },
  price: {
    type: [Number],
    required: true,
  },
});

module.exports = mongoose.model("Items", ItemSchema);
